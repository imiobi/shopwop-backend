<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Models\Brand;

class BrandController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function blank()
    {
        return new Brand;
    }

    public function index() 
    {
        $aBrands = Brand::where('deleted_at', '=', null)->get();
    
    
            if(!$aBrands) {
                return response()->json([
                    'status' => false,
                    'message' => 'Brand listing error',
                ]);
            }
    
            return response()->json([
                'status' => true,
                'message' => 'Brand listing',
                'data' => $aBrands
            ]);
    }



     /**
      * *BRAND SINGLE
     *
     * @param [int] $id
     * @return void
     */
   public function show($id)
   {

    if(!is_numeric($id)) {
        abort('404');
    }

    try {
        $oBrand = Brand::find($id);
        if(!$oBrand) {
            throw new \Exception("Invalid Id");
        }
    } catch(\Exception $e) {
        return response()->json([
            'status' => false,
            'message' => $e->getMessage()
        ]);
    }
    
    return response()->json([
        'status' => true,
        'message' => 'Brand single',
        'data' => $oBrand
    ]);

   }

   /**
     * *DELETE BRAND
     * 
     * this function deletes a brand based on the given ID
     * 
     *
     * @param [int] $id
     * @return redirect || JSON
     */
    public function delete($id)
    {
        if(!is_numeric($id)) {
            abort('404');
        }

        try {

            $oBrand = Brand::find($id);
            if(!$oBrand) {
                throw new \Exception(" Invalid ID");
            }

            $deleteBrand = $oBrand->delete();

            if(!$deleteBrand ) {
                throw new \Exception("Invalid Id");
            } 
                
            return response()->json([
                    'status' => true,
                    'redirect_url' => '/brands',
                    'message' => 'Record deleted successfully !!!'
                ]);

        } catch(\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);

        }
        
    }


     /**
     * *CREATE Brand
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

        $data = $request->all();

        $oBrand = $this->blank();
        $response = $oBrand->store($data);


        if($response instanceof \Illuminate\Validation\Validator) {
            return response()->json([
                'status' => false,
                'message' => 'please fix all errors',
                'errors' => $response->errors()->toArray(),
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'Brand created successfully',
            'redirect_url' => '/brands',
        ]);
    }


    /**
     * *UPDATE BRAND
     *
     * @param Request $request
     * @return json
     */
    public function update($id, Request $request)
    {
        
        if(!is_numeric($id)) {
            abort('404');
        }
        
        
        $data = $request->all();
        
        $oBrand = Brand::find($id);
        $response = $oBrand->store($data);
        
        
        if($response instanceof \Illuminate\Validation\Validator) {
            return response()->json([
                'status' => false,
                'message' => 'please fix all errors',
                'errors' => $response->errors()->toArray(),
                ]);
            }

            return response()->json([
                'status' => true,
                'message' => 'Brand updated successfully',
                'redirect_url' => '/brands',
            ]);
        }


        public function countProductsByBrand()
        {
            $aCountProducts = \DB::select("select count(p.id) as count_products, b.brand_name as brand_name
            from brands as b
            inner join products as p
            on p.brand_id = b.id
            where b.deleted_at is null and p.is_active = 1 and p.deleted_at is null
            group by brand_name
            order by count_products desc;");

            if(!$aCountProducts) {
                return response()->json([
                    "status" => true,
                    "message" => "No Product Found"
                ]);
            }

            return response()->json([
                "status" => true,
                "message" => "Products listing by brand name",
                "data" => $aCountProducts
            ]);
        }



        public function getBrandsStats() {

            $aTotalBrandCount['stats'] = Brand::select(DB::raw('count(id) as count'))
                ->where('deleted_at', '=', null)->get();

            $aTotalBrandCount['title'] = "Total Brands";

            $aDeletedBrandCount['stats'] = Brand::select(DB::raw('count(id) as count'))->onlyTrashed()->get();

            $aDeletedBrandCount['title'] = "Deleted Brands";

            return response()->json([
                'status'=> true,
                'data' => [
                    'aTotalBrandCount' => $aTotalBrandCount,
                    'aDeletedBrandCount' => $aDeletedBrandCount
                ]
            ]);
        }
}
