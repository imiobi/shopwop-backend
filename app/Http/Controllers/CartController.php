<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;

use App\Models\Cart;

class CartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


     /**
     * *CART LISTING
     * 
     * this function will list out all products of cart of authenticated user
     *
     * @param [int] $customerId
     * @return void
     */
    public function cartListing($customerId)
    {
        if(!is_numeric($customerId)) {
            return response()->json([
                'status' => false,
                'message' => 'Id should be numeric'
            ]);
        }

        try {
            $oUser = Cart::where([
                                    'customer_id' => $customerId,
                                    'is_ordered' => 0
                                ])
                            ->with('cartProduct')->get();

            if(!$oUser) {
                throw new \Exception('Invalid customer id');
            }

            return response()->json([
                'status' => true,
                'message' => 'Authenticated customer cart products listing',
                'data' => $oUser
            ]);

        } catch(\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }

       
    }


    /**
     * *ADD PRODUCT TO CART
     *
     * if same cart already exist then it's quantity will be updated
     * otherwise it'll create new cart
     * 
     * @param Request $request
     * @return mix
     */
    public function addCart(Request $request)
    {
            $data = $request->all();
            $data['is_ordered'] =  0;

            $validationRules = [
                'product_id' => 'required|integer',
                'customer_id' => 'required|integer',
                'variants' => 'nullable',
                'quantity_ordered' => 'required|integer:min:1',
                'is_ordered' => 'required|boolean',
            ];

            $oValidator = Validator::make($data, $validationRules);

            if($oValidator->fails()){

                return response()->json([
                    'status' => false,
                    'message' => 'please fix all errors',
                    'errors' => $oValidator->errors()->toArray(),
                ]);
            }

            $query = Cart::where([
                'product_id' => $data['product_id'],
                'customer_id' => $data['customer_id'],
                'variants' => $data['variants'] ?? null,
                'is_ordered' => 0
            ]);

            if($query->first()) {
                $quantity = $query->addSelect('quantity_ordered')->pluck('quantity_ordered')->first();
                $query->update( ['quantity_ordered' => ($quantity + $data['quantity_ordered'])] );

            } else {
                $oCart = new Cart;
                $oResponse = $oCart->store($data);

                if($oResponse instanceof \Illuminate\Validation\Validator) {
                    return response()->json([
                        'status' => false,
                        'message' => 'please fix all errors',
                        'errors' => $oResponse->errors()->toArray(),
                    ]);
                }
            }

            return response()->json([
                'status' => true,
                'message' => 'Item added to cart successfully',
                'redirect_url' => '/',
            ]);
    }



    /**
     * *UPDATE CART
     * 
     * This function will update cart items in bulk
     *
     * @param Request $request
     * @return void
     */
    public function updateCart(Request $request)
    {

        $aData = $request->all();


        $errors = array();
        try {
            foreach($aData as $cart) {

                if($cart['quantity_ordered'] <= 0) {
                    Cart::where('id', $cart['id'])->delete();
                } else {
                    $oCart = Cart::where([
                    'id' => $cart['id']
                    ])->update(['quantity_ordered' => $cart['quantity_ordered']]);

                    if(!$oCart) {
                        $errors[] = $cart['id'];
                    }
                }
                
            }


            if(count($errors) == 0) {
                return response()->json([
                    'status' => true,
                    'message' => "All cart updated successfully",
                    'redirect_url' => '/'
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => "following products not updated in cart ". explode(',', array_values($errors))
            ]);
        }

        }catch( \Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }


    /**
     * THis function will delete cart of given id
     *
     * @param [int] $cartId
     * @return void
     */
    public function delete($cartId)
    {
        if(!is_numeric($cartId)) {
            return response()->json([
                'status' => false,
                'message' => 'Id should be numeric'
            ]);
        }

        try {
            $oCart = Cart::where('id', $cartId)->delete();

            if(!$oCart) {
                throw new \Exception('Given Card id is not valid ');
            }

            return response()->json([
                'status' => true,
                'message' => 'Cart Deleted Successfully !!!'
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
        
    }
}
