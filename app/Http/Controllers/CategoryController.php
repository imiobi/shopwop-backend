<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use DB;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function blank()
    {
        return new Category;
    }

    public function index()
    {
        $aCategories = Category::where('is_active', 1)->where('deleted_at', '=', null)->get();


        if (!$aCategories) {
            return response()->json([
                'status' => false,
                'message' => 'Category listing error',
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'Category listing',
            'data' => $aCategories
        ]);
    }


    /**
     * *CATEGORY SINGLE
     *
     * @param [int] $id
     * @return void
     */
    public function show($id)
    {

        if (!is_numeric($id)) {
            abort('404');
        }

        try {
            $oCategory = Category::find($id);
            if (!$oCategory) {
                throw new \Exception("Invalid Id");
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'Category single',
            'data' => $oCategory
        ]);

    }


    /**
     * *DELETE Category
     *
     * this function deletes a Category based on the given ID
     *
     *
     * @param [int] $id
     * @return redirect || JSON
     */
    public function delete($id)
    {
        if (!is_numeric($id)) {
            abort('404');
        }

        try {

            $oCategory = Category::find($id);
            if (!$oCategory) {
                throw new \Exception(" Invalid ID");
            }

            $deleteCategory = $oCategory->delete();

            if (!$deleteCategory) {
                throw new \Exception("Invalid Id");
            }

            return response()->json([
                'status' => true,
                'redirect_url' => '/categories',
                'message' => 'Record deleted successfully !!!'
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);

        }

    }


    /**
     * *CREATE Category
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

        $data = $request->all();

        $oCategory = $this->blank();
        $response = $oCategory->store($data);

        if ($response instanceof \Illuminate\Validation\Validator) {
            return response()->json([
                'status' => false,
                'message' => 'please fix all errors',
                'errors' => $response->errors()->toArray(),
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'Category created successfully',
            'redirect_url' => '/categories',
        ]);
    }

    /**
     * *UPDATE Category
     *
     * @param Request $request
     * @return json
     */
    public function update($id, Request $request)
    {

        if (!is_numeric($id)) {
            abort('404');
        }


        $data = $request->all();

        $oCategory = Category::find($id);
        $response = $oCategory->store($data);


        if ($response instanceof \Illuminate\Validation\Validator) {
            return response()->json([
                'status' => false,
                'message' => 'please fix all errors',
                'errors' => $response->errors()->toArray(),
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'Category updated successfully',
            'redirect_url' => '/categories',
        ]);
    }


    public function countProductsByCategory()
    {
        $aCountProducts = \DB::select("select count(p.id) as count_products, c.category_name as category_name, c.id as category_id
            from categories as c
            inner join products as p
            on p.category_id = c.id
            where c.is_active = 1 and c.deleted_at is null and p.is_active = 1 and p.deleted_at is null
            group by category_name, category_id
            order by count_products desc");

        if (!$aCountProducts) {
            return response()->json([
                "status" => false,
                "message" => "No Product Found"
            ]);
        }

        return response()->json([
            "status" => true,
            "message" => "Products listing by category name",
            "data" => $aCountProducts
        ]);


    }


    public function top5Categories()
    {
        $aTop5Categories = DB::select('select c.id, c.category_name, count(p.category_id) as product_category
                                    from categories as c
                                    inner join products as p
                                    on p.category_id = c.id 
                                    where c.is_active = 1 and c.deleted_at is null and p.deleted_at is null and p.is_active = 1
                                    group by p.category_id
                                    order by product_category desc
                                    limit 5');

        if (!$aTop5Categories) {
            return response()->json([
                "status" => false,
                "message" => "No Category Found"
            ]);
        }

        return response()->json([
            "status" => true,
            "message" => "Top 5 Categories",
            "data" => $aTop5Categories
        ]);
    }


    public function getCategoriesStats() {

        $aTotalCategoriesCount['stats'] = Category::select(DB::raw('count(id) as count'))
            ->where('deleted_at', '=', null)->get();

        $aTotalCategoriesCount['title'] = "Total Categories";

        $aActiveCategoriesCount['stats'] = Category::select(DB::raw('count(id) as count'))
            ->where('is_active', '=', 1)->where('deleted_at', '=', null)->get();
        $aActiveCategoriesCount['title'] = "Active Categories";

        $aInActiveCategoriesCount['stats'] = Category::select(DB::raw('count(id) as count'))
            ->where('is_active', '=', 0)->where('deleted_at', '=', null)->get();
        $aInActiveCategoriesCount['title'] = "In-Active Categories";

        $aDeletedCategoriesCount['stats'] = Category::select(DB::raw('count(id) as count'))
            ->onlyTrashed()->get();
        $aDeletedCategoriesCount['title'] = "Deleted Categories";

        return response()->json([
            'status' => true,
            'data' => [
                'aTotalCategoriesCount' => $aTotalCategoriesCount,
                'aActiveCategoriesCount' => $aActiveCategoriesCount,
                'aInActiveCategoriesCount' => $aInActiveCategoriesCount,
                'aDeletedCategoriesCount' => $aDeletedCategoriesCount,
            ],
        ]);
    }


    public function productsByCategory($categoryId)
    {
        if(!is_numeric($categoryId)) {
            return response()->json([
                'status' => false,
                'message' => "Id should be numeric"
            ]);
        }


        try {

            $oCategory = Category::with('products')->where([
                'id' => $categoryId,
                'is_active' => 1,
                'deleted_at' => null
            ])->find($categoryId);
            
            if(!$oCategory) {
                throw new Exception ('Invalid id');
            }

        } catch(\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ]);
        }


        return response()->json([
            'status' => true,
            'message' => 'Products Listing by Category',
            'data' => $oCategory
        ]);
    }
}
