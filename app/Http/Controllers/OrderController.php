<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Cart;
use App\Models\User;
use App\Models\Product;
use App\Models\Payment;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public $aOrderStatuses = [
        'Pending',
        'Cancelled', 
        'On-Hold',
        'Dispatch',
        'Shipped',
        'Completed'
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

   public function blank()
   {
       return new Order;
   }


   /**
    * *Order Statuses
    *
    * @return mix
    */
    public function getOrderStatuses()
    {
        return response()->json([
                'status' => false,
                'message' => 'order status listing',
                'data' => $this->aOrderStatuses
            ]);
    }


    /**
     * *ORDER listing
     *
     * @return void
     */
    public function index()
    {
        $aOrders = Order::with('products')->get();

        if(!$aOrders) {
            return response()->json([
                'status' => false,
                'message' => 'Order listing error',
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'Order listing',
            'data' => $aOrders
        ]);
    }


    /**
     * *ORDER Show
     *
     * @return void
     */
    public function show($id)
    {
        $aOrders = Order::with('payment', 'products')->find($id);


        if(!$aOrders) {
            return response()->json([
                'status' => false,
                'message' => 'Order show page error',
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'Order Single page display',
            'data' => $aOrders
        ]);
    }


    /**
     * *ORDER create
     * 
     * @return json
     */
    public function create(Request $request)
    {
//        return $request->all();
        $user = $request->only([
            'first_name', 
            'last_name', 
            'contact_no',
            'email', 
            'postal_code', 
            'state',
            'city',
            'address',
            'shipped_address',
            'customer_id',
            'comments',
            ]);

        $validationRules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'nullable|email',
            'contact_no' => 'required',
            'state' => 'required',
            'city' => 'required',
            'comments' => 'nullable',
            'postal_code' => 'required|integer'
        ];

        
        $validator  = Validator::make($user, $validationRules);


        if($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'please fix all errors',
                'errors' => $validator->errors()->toArray(),
            ]);
        }

        //if user has changed the shipping address
        $newData = [
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'contact_no' => $user['contact_no'],
            'postal_code' => $user['postal_code'],
            'state' => $user['state'],
            'city' => $user['city'],
            'address' => $user['address'],

        ];

        if(isset($user['shipped_address'])) {
            $newData['shipped_address'] = $user['shipped_address'];
        }

        $oUserUpdate = User::where('id', $user['customer_id'])->update($newData);


        if(!$oUserUpdate) {
            return response()->json([
                'status' => false,
                'message' => 'Customer Detail not updated',
            ]);
        }

        $data = $request->except([
            'first_name', 
            'last_name', 
            'contact_no',
            'email', 
            'postal_code', 
            'state',
            'city',
            'address',
            'shipped_address'
        ]);

        $last_insert_id = \DB::table('orders')->select('id')
                            ->orderBy('id', 'DESC')->take(1)->pluck('id')->first();

        $data['order_number'] = '#ORD'. date('dmy') . '-' . ($last_insert_id - 1);
        $data['status'] = 'Pending';

        $oOrder = $this->blank();
        $response = $oOrder->store($data);

        if($response instanceof \Illuminate\Validation\Validator) {
            return response()->json([
                'status' => false,
                'message' => 'please fix all errors',
                'errors' => $response->errors()->toArray(),
            ]);
        }

        $oUser = User::find($data['customer_id']);
        $aProductIDs = $oUser->carts()->where('is_ordered',0)
                            ->addSelect('product_id', 'quantity_ordered')
                            ->pluck('quantity_ordered', 'product_id')
                            ->toArray();
        

        $aProducts = Product::whereIn('id', array_keys($aProductIDs))->get();

        foreach($aProducts as  $oProduct) {

            $quantityOrdered = in_array($oProduct->id, array_keys($aProductIDs)) ? $aProductIDs[$oProduct->id] : 1;

            OrderProduct::insert([
                'order_id' => $response->id,
                'product_id' => $oProduct->id,
                'quantity_ordered' => $quantityOrdered,
                'amount' => $oProduct->price * $quantityOrdered
            ]);

            Cart::where([
                'customer_id' => $request->customer_id,
                'product_id' =>  $oProduct->id,
                'is_ordered' => 0
                ])->update([
                    'is_ordered' => 1
                ]);
            
            $iAvailableStockQuantity = Product::where('id', $oProduct->id)
                                                ->addSelect('stock_quantity')
                                                ->pluck('stock_quantity')
                                                ->first();

            Product::where('id', $oProduct->id)->update([
                'stock_quantity' => ($iAvailableStockQuantity - $quantityOrdered)
            ]);

        }

        $orderTotalAmount = OrderProduct::select('amount')
                            ->where('order_id', $response->id)
                            ->sum('amount');

        Payment::insert([
            'order_id' => $response->id,
            'payment_method_id' => 1,
            'total_amount' =>    ($orderTotalAmount + 50), //50 figure is shipping charges
            'is_paid' => 0
        ]);

        return response()->json([
            'status' => true,
            'message' => 'Order created successfully with order number ' . $response->order_number,
            'redirect_url' => '/',
        ]);
    }


    /**
     * *UPDATE Order
     * @return mix
     */
    public function update($id, Request $request)
    {
        if(!is_numeric($id)) {
            abort('404');
        }

        $data = $request->except(['payment_date', 'is_paid', 'total_amount','_token', '_method']);

        $oOrder = Order::find($id);
        $oResponse = $oOrder->store($data);

        if($oResponse instanceof \Illuminate\Validation\Validator) {
            return response()->json([
                'status' => false,
                'message' => 'please fix all errors',
                'errors' => $oResponse->errors()->toArray(),
            ]);
        }

        payment::where(['order_id' => $id])->update([
            'payment_method_id' => 1,
            'is_paid' => $request->is_paid,
            'payment_date' => $request->payment_date,
            'total_amount' => $request->total_amount
        ]);

        return response()->json([
            'status' => true,
            'message' => 'Order updated successfully',
            'redirect_url' => '/',
        ]);
    }

   
    public function getOrderStatusCount() {

        $aOrderStatusCount = [];

        foreach ($this->aOrderStatuses as $sStatus) {
            $aOrderStatusCount[$sStatus] = DB::table('orders')
                                        ->select(DB::raw('count(id) as count'))
                                        ->where('status', '=', $sStatus)
                                        ->count();

        }

        if(count($aOrderStatusCount) <1 ) {
            return response()->json([
                'status'=> false,
                'message'=> "No status count found",
            ]);
        }

        return response()->json([
            'status'=> true,
            'message'=> "Order status count succesfull",
            'data'=> $aOrderStatusCount,
        ]);
       

    }




}