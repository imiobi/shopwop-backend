<?php

namespace App\Http\Controllers;
use App\Models\Payment;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * *Return object of model
     *
     * @return void
     */
    public function blank()
    {
        return new Payment;
    }

    /**
     * *Payment Listing
     *
     * @return mix
     */
    public function index()
    {
        $aPayments = Payment::where('deleted_at', null)->get();

        if(!$aPayments) {
            return response()->json([
                'status' => false,
                'message' => 'Payment listing error',
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'Payment listing',
            'data' => $aPayments,
        ]);
    }
}
