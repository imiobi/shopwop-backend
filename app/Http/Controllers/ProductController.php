<?php

namespace App\Http\Controllers;
use App\Models\Attachment;
use App\Models\Brand;
use Illuminate\Http\Request;
use DB;
use App\Models\Product;
use App\Models\User;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function blank()
    {
        return new Product;
    }


    /**
     * *PRODUCT LISTING
     *
     * @return void
     */
    public function index(Request $request)
    {

        $pageNumber = $request->page;
        $perPage = $request->perPage;
        $sortBy = $request->sortBy;
        $orderBy = $request->orderBy;

        $query = Product::LeftJoin('categories', function($join){
            $join->on('products.category_id', '=', 'categories.id');
            })->leftJoin('brands', function($join){
                $join->on('products.brand_id', '=', 'brands.id');
            })->addSelect('products.*')->where([
                    'products.is_active' => 1,
                    ]);
                    
        if(is_null($pageNumber) && is_null($perPage)) {
            $aProducts = $query->get();
        } else {
            $aProducts = $query->orderBy($sortBy, $orderBy)->paginate($perPage, ['*'], 'page', $pageNumber );
        }
       

        if(!$aProducts) {
            return response()->json([
                'status' => false,
                'message' => 'product listing error',
            ]);
        }

        $pageNumber = $pageNumber ?? 1;
        return response()->json([
            'status' => true,
            'message' => 'product listing with page number' . $pageNumber,
            'data' => $aProducts,
        ]);
    }

    /**
      * *PRODUCT SINGLE
     *
     * @param [int] $id
     * @return void
     */
   public function show($id)
   {

    if(!is_numeric($id)) {
        abort('404');
    }

    try {
        $oProduct = Product::find($id);
        if(!$oProduct) {
            throw new \Exception("Invalid Id");
        }
    } catch(\Exception $e) {
        return response()->json([
            'status' => false,
            'message' => $e->getMessage()
        ]);
    }

    $countViews = Product::where('id', $id)->addSelect('view_count')->pluck('view_count')->first();

    Product::where('id', $id)->update([
        'view_count' => $countViews + 1
    ]);

    return response()->json([
        'status' => true,
        'message' => 'product single',
        'data' => $oProduct
    ]);

   }


    /**
     * *DELETE PRODUCT
     * 
     * this function deletes a product based on the given ID
     * 
     *
     * @param [int] $id
     * @return redirect || JSON
     */
    public function delete($id)
    {
        if(!is_numeric($id)) {
            abort('404');
        }

        try {

            $oProduct = Product::find($id);
            if(!$oProduct) {
                throw new \Exception(" Invalid ID");
            }

            $deleteProduct = $oProduct->delete();

            if(!$deleteProduct ) {
                throw new \Exception("Invalid Id");
            } 
                
            return response()->json([
                    'status' => true,
                    'redirect_url' => '/products',
                    'message' => 'Record deleted successfully !!!'
                ]);

        } catch(\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);

        }
        
    }

    /**
     * *CREATE PRODUCT
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

        $data = $request->except('image');

        $last_insert_id = \DB::table('users')->select('id')
                            ->orderBy('id', 'DESC')->take(1)->pluck('id')->first();

        $data['product_code'] = '#PRD'. '-' . ($last_insert_id - 1);

        $oProduct = $this->blank();
        $response = $oProduct->store($data);

        if($response instanceof \Illuminate\Validation\Validator) {
            return response()->json([
                'status' => false,
                'message' => 'please fix all errors',
                'errors' => $response->errors()->toArray(),
            ]);
        }
        Attachment::insert([
            'image_name' => $request->image,
            'image_path' => $request->image,
            'product_id' => $response->id
        ]);


        return response()->json([
            'status' => true,
            'message' => 'product created successfully',
            'redirect_url' => '/products',
        ]);
    }


    
    
    
    /**
     * *UPDATE PRODUCT
     *
     * @param Request $request
     * @return json
     */
    public function update($id, Request $request)
    {
        if(!is_numeric($id)) {
            abort('404');
        }


        $data = $request->except('image');

        $oProduct = Product::find($id);
        $response = $oProduct->store($data);


        Attachment::where('product_id', '=', $response)->update([
            'image_path' => $request->image,
            'image_name' => $request->image,
        ]);


        
        if($response instanceof \Illuminate\Validation\Validator) {
            return response()->json([
                'status' => false,
                'message' => 'please fix all errors',
                'errors' => $response->errors()->toArray(),
                ]);
            }

            return response()->json([
                'status' => true,
                'message' => 'product updated successfully',
                'redirect_url' => '/products',
            ]);
        }


        /**
         * This function will list out all products which are in wish list by customer_Id
         *
         * @return void
         */
        public function wishLists($userId)
        {
            if(!is_numeric($userId)) {
                return response()->json([
                    'status' => FALSE,
                    'message' => "ID can not be string",
                ]);
            }

            try {

                $oUser = User::find($userId);

                if(!$oUser) {
                    throw new \Exception('Invalid user id');
                }

                $aProducts = $oUser->wishListProducts()
                                    ->select('products.id','product_code', 'product_title', 'price')->get();
                
                if($aProducts && count($aProducts) > 0) {
                    return response()->json([
                        'status' => true,
                        'message' => 'All wish lists products of Authenticated users',
                        'data' => $aProducts
                    ]);
                }
            } catch(\Exception $e) {
                return response()->json([
                    'status' => FALSE,
                    'message' => $e->getMessage(),
                ]);
            }


        }

        
       /***
        *  *IMIDUDE
        * Brand and Category Listing 
        */
        public function getBrandAndCategory() {
            $data = DB::table('brands')
                    ->select('brands.id as  brand_id', 'brands.brand_name as brand_name', 'categories.id as category_id', 'categories.category_name as category_name')
                    ->join('products', 'brands.id', '=', 'products.brand_id')
                    ->join('categories', 'categories.id', '=', 'products.category_id')
                    ->get();
    
            if(!$data) {
                return response()->json([
                    'status'=> false,
                    'message'=> 'brand and categories not found',
                ]);
            }
    
            return response()->json([
                'status'=> true,
                'message'=> 'Branch and Category data',
                'data'=> $data,
            ]);
        }


        public function top4RecentProducts()
        {
            $aTop4RecentProducts = Product::LeftJoin('categories', function($join){
                                    $join->on('products.category_id', '=', 'categories.id');
                                    })->leftJoin('brands', function($join){
                                        $join->on('products.brand_id', '=', 'brands.id');
                                    })->addSelect('products.*')->where([
                                                                'products.is_active' => 1,
                                                                'products.deleted_at' => null,
                                                                'categories.is_active' => 1,
                                                                'categories.deleted_at' => null,
                                                                'brands.deleted_at' => null
                                                                ])
                                    ->orderBy('created_at', 'desc')->take(4)->get();

            if(!$aTop4RecentProducts) {
                return response()->json([
                    "status" => false,
                    "message" => "No Product Found"
                ]);
            }

            return response()->json([
                "status" => true,
                "message" => "Top 4 Recent Products",
                "data" => $aTop4RecentProducts
            ]);
        }

        public function top12ViewedProducts()
        {
            $aTop12MostViewProducts = Product::LeftJoin('categories', function($join){
                $join->on('products.category_id', '=', 'categories.id');
                })->leftJoin('brands', function($join){
                    $join->on('products.brand_id', '=', 'brands.id');
                })->addSelect('products.*')->where([
                                            'products.is_active' => 1,
                                            'products.deleted_at' => null,
                                            'categories.is_active' => 1,
                                            'categories.deleted_at' => null,
                                            'brands.deleted_at' => null
                                            ])
                ->orderBy('products.view_count', 'desc')->take(12)->get();;

            if(!$aTop12MostViewProducts) {
                return response()->json([
                    "status" => false,
                    "message" => "No Product Found"
                ]);
            }

            return response()->json([
                "status" => true,
                "message" => "Top 12 Most Viewed Products",
                "data" => $aTop12MostViewProducts
            ]);
        }

        public function top4SoldProducts()
        {
            $aGetTopSoldProductIds = DB::select("select id from (select p.id as id, sum(op.quantity_ordered) as quantity_ordered , p.product_title  as product_title
            from products as p
            inner join order_products as op
            on p.id = op.product_id 
            inner join orders as o
            on o.id = op.order_id
            where p.is_active = 1 and p.deleted_at is null
            group by p.id 
            order by quantity_ordered desc
            limit 4) as topProducts");

            $aGetTopSoldProductIds = array_column($aGetTopSoldProductIds, 'id');

            $aTop4SoldProducts = Product::select('*')->whereIn('id', $aGetTopSoldProductIds)->get();

            if(!$aTop4SoldProducts) {
                return response()->json([
                    "status" => false,
                    "message" => "No Product Found"
                ]);
            }

            return response()->json([
                "status" => true,
                "message" => "Top 4 Sold Products",
                "data" => $aTop4SoldProducts
            ]);
        }


        public function searchProducts(Request $request) {

            $sKeyword = $request->input('q');
            $pageNumber = $request->page;
            $perPage = $request->perPage;
            $sortBy = $request->sortBy;
            $orderBy = $request->orderBy;

            try {
                    $aResults = Product::LeftJoin('categories', function($join){
                        $join->on('products.category_id', '=', 'categories.id');
                        })->leftJoin('brands', function($join){
                            $join->on('products.brand_id', '=', 'brands.id');
                        })->addSelect('products.*')->where([
                                                    'products.is_active' => 1,
                                                    'products.deleted_at' => null,
                                                    'categories.is_active' => 1,
                                                    'categories.deleted_at' => null,
                                                    'brands.deleted_at' => null
                                                    ])->where('products.product_title', 'LIKE', '%' . $sKeyword . '%')
                                        ->orWhere('products.short_description', 'LIKE', '%' . $sKeyword . '%')
                                        ->orWhere('products.specifications', 'LIKE', '%' . $sKeyword . '%')
                                        ->orWhere('products.product_description', 'LIKE', '%' . $sKeyword . '%')
                                        ->orWhere('products.price', 'LIKE', '%' . $sKeyword . '%')
                                        ->orderBy($sortBy, $orderBy)
                                        ->paginate($perPage, ['*'], 'page', $pageNumber );

                    if(!$aResults) {
                        return response()->json([
                            'status' => false,
                            'message' => 'No Product Found',
                            'data' => [],
                        ]);
                    }

                    if($aResults && count($aResults) > 0) {
                        return response()->json([
                            'status' => true,
                            'message' => "Search products of query $sKeyword",
                            'data' => $aResults,
                        ]);
                    }
                } catch(\Exception $e) {
                    return response()->json([
                        'status' => false,
                        'message' => $e->getMessage(),
                    ]);
                }

        }


        public function productsStats() {


            $aTotalProductCount['stats'] = Product::select(DB::raw('count(id) as count'))
                ->where('deleted_at', '=', null)->get();
            $aTotalProductCount['title']= "Total Products";

            $aActiveProductCount['stats'] = Product::select(DB::raw('count(id) as count'))
                ->where('is_active', '=', 1)->where('deleted_at', '=', null)->get();
            $aActiveProductCount['title']= "Active Products";


            $aInActiveProductCount['stats'] = Product::select(DB::raw('count(id) as count'))
                ->where('is_active', '=', 0)->where('deleted_at', '=', null)->get();
            $aInActiveProductCount['title']= "In-Active Products";



            $aDeletedProductCount['stats'] = Product::select(DB::raw('count(id) as count'))
                ->onlyTrashed()->get();

            $aDeletedProductCount['title']= "Deleted Products";


            return response()->json([
                'status' => true,
                'data' => [
                    'aTotalProductCount'=> $aTotalProductCount,
                    'aActiveProductCount' => $aActiveProductCount,
                    'aInActiveProductCount' => $aInActiveProductCount,
                    'aDeletedProductCount' => $aDeletedProductCount,
                ]
            ]);

        }

    }
