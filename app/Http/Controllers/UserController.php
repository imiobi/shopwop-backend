<?php

namespace App\Http\Controllers;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function listUsers($type)
    {
        if($type == 'admins') {
            $query  = User::where('is_admin', 1);
        } else if ($type == 'customers') {
            $query  = User::where('is_admin', 0);
        } else {
            $query  = User::whereIn('is_admin', [1, 0] );
        }

        $aUsers = $query->get();

        if($aUsers && count($aUsers) > 0) {
            return response()->json([
                'status' => true,
                'message' => "$type users listing",
                'data' => $aUsers
            ]);
        }

        return response()->json([
            'status' => false,
            'message' => "No Data Found"
        ]);

    }


    public function show($id)
    {
        if(!is_numeric($id)) {
            return response()->json([
                'status' => false,
                'message' => "Id should be numeric"
            ]);
        }

        try {
            $oUser = User::where('id',$id)->where('deleted_at', null)->first();
            
            if(!$oUser) {
                throw new \Exception('Invalid user id');
            }

            return response()->json([
                'status' => true,
                'message' => 'User single page',
                'data' => $oUser
            ]);

        } catch(\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }


    public function update($id, Request $request)
    {

        if(!is_numeric($id)) {
            return response()->json([
                'status' => false,
                'message' => "Id should be numeric"
            ]);
        }
        
        $data = $request->except(['_method', '_token']);

        try {
            $oUser = User::find($id);
            $response = $oUser->store($data);

            
            if($response instanceof \Illuminate\Validation\Validator) {
                return response()->json([
                    'status' => false,
                    'message' => 'please fix all errors',
                    'errors' => $response->errors()->toArray(),
                ]);
            }

            return response()->json([
                'status' => true,
                'message' => 'User updated successfully',
            ]);

        } catch(\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }


    public function delete($id)
    {
        if(!is_numeric($id)) {
            return response()->json([
                'status' => false,
                'message' => "Id should be numeric"
            ]);
        }

        try {

            $oUser = User::find($id);
            if(!$oUser) {
                throw new \Exception(" Invalid ID");
            }

            $deleteUser = $oUser->delete();

            if(!$deleteUser ) {
                throw new \Exception("Invalid Id");
            } 
                
            return response()->json([
                    'status' => true,
                    'redirect_url' => '/',
                    'message' => 'User deleted successfully !!!'
                ]);

        } catch(\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);

        }
    }


    public function orderHistory($customerId) {



        $aOrders = Order::where('customer_id', '=', (int) $customerId)->get();

        if(!$aOrders) {
            return response()->json([
                'status' => false,
                'message' => 'Order show page error',
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'User Order History display',
            'data' => $aOrders
        ]);
    }
    
}
