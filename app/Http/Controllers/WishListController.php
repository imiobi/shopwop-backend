<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\WishList;
use Illuminate\Http\Request;


class WishListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function storeWishList(Request $request) {


        $iCountWishList = WishList::where('product_id', '=', $request->product_id)->where('customer_id', $request->customer_id)->count();

        if ($iCountWishList > 0) {

            return response()->json([
               'status'=> false,
               'message'=> 'Product already added in wishlist',
            ]);

        } else {

            $oWishlist = new WishList();

            $oWishlist->product_id = $request->product_id;
            $oWishlist->customer_id = $request->customer_id;

            $oWishlist->save();


            return response()->json([
                'status' => true,
                'message' => 'wishlist created successfully',
                'redirect_url' => '/products',
            ]);
        }








//        $response = $oWishlist->store($data);


//        if ($response instanceof \Illuminate\Validation\Validator) {
//            return response()->json([
//                'status' => false,
//                'message' => 'please fix all errors',
//                'errors' => $response->errors()->toArray(),
//            ]);
//        }
//
//        return response()->json([
//            'status' => true,
//            'message' => 'product created successfully',
//            'redirect_url' => '/products',
//        ]);
    }


    public function index($iCustomerId) {

        $aWishLIst = WishList::where('customer_id', '=', $iCustomerId)->get();

        if(!$aWishLIst) {
            return response()->json([
                'status'=> false,
                'message'=> "failed to get wishlist",
            ]);
        }

        return response()->json([
            'status'=> true,
            'message'=> "Success",
            'data'=> $aWishLIst,
        ]);


    }

    public function delete(Request $request)
    {
        try {

          $query =   WishList::where([
              'customer_id' => $request->customer_id,
              'product_id' => $request->product_id
          ]);

          if(!$query->first()) {
              throw new \Exception('Invalid product or customer id');
          }

          if($query->delete()) {
              return response()->json([
                  'status' => true,
                  'message' => 'Wishlist is removed successfully!',
              ]);
          }
        } catch(\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }


}
