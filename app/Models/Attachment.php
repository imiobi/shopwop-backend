<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Base;
use App\Models\Product;

class Attachment extends Model
{
    use Base;
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'attachments';

    protected $fillable = [
        'product_id',
        'image_path',
        'image_name',
    ];


    protected $validationRules = [
        'product_id' => 'required|integer',
        'image_path' => 'required|mimes:jpeg,jpg,png|max:2048|dimensions:min_width=800,max_width=3000,min_height:400,max_height=1700',
        'image_name' => 'required',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    
}
