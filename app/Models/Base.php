<?php

namespace App\Models;
use Validator;


trait Base {

    public function getRulesForCreate()
    {
        return $this->validationRules;
    }


    public function getRulesForUpdate()
    {
        return $this->validationRules;
    }

    public function fillables()
    {
        return $this->fillable;
    }

    public function dateFormat($date)
    {
        return date('d/m/Y h:i:s A', strtotime($date));
    }

    public function store($data = array())
    {

        $validationRules = $this->id ? $this->getRulesForUpdate() : $this->getRulesForCreate();
        
        if($validationRules && count($validationRules) > 0) {            
            $validator = Validator::make($data, $validationRules);       
            if($validator->fails()) {
                return $validator;
            }
        }

        if($this->id) {

            $this->where('id', $this->id)->update($data);
            return $this->id;
        }else {
            $oEntityResponse = $this->create($data);
            return $oEntityResponse;
        }

    }

}