<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Base;
use App\Models\Product;
use Illuminate\Validation\Rule;

class Brand extends Model
{
    use Base;
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'brands';

    protected $fillable = [
        'brand_name',
        'contact_no',
        'email',
        'country'
    ];

    protected $validationRules = [
        'brand_name' => 'required',
        'contact_no' => 'required|min:10|max:18',
        'email' => 'required|email|unique:brands,email',
        'country' => 'required',
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'brand_id');
    }

    public function getRulesForUpdate()
    {
        $this->validationRules['email'] =  [
                                                'required',
                                                Rule::unique('brands', 'email')->ignore($this->id),
                                            ];

        return $this->validationRules;
    }
    
}
