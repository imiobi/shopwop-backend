<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Base;
use App\Models\User;
use App\Models\Product;

class Cart extends Model
{
    use Base;

    protected $primaryKey = 'id';
    protected $table = 'carts';

    protected $fillable = [
        'product_id',
        'customer_id',
        'variants',
        'quantity_ordered',
        'is_ordered'
    ];

    protected $validationRules = [
        'product_id' => 'required|integer',
        'customer_id' => 'required|integer',
        'variants' => 'nullable',
        'quantity_ordered' => 'required|integer|min:1',
        'is_ordered' => 'required|boolean',
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }


    public function cartProduct()
    {
        return $this->hasOne(Product::class, 'id', 'product_id')->select('id','price','product_title');
    }
    
}
