<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Base;
use App\Models\Product;



class Category extends Model
{
    use Base;
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'categories';

    protected $fillable = [
        'category_name',
        'parent_category_id',
        'description',
        'is_active'
    ];


    protected $validationRules = [
        'category_name' => 'required|string',
        'parent_category_id' => 'nullable|integer',
        'description' => 'required|string',
        'is_active' =>  'required|boolean'
    ];

    public function products()
    {
       return  $this->hasMany(Product::class, 'category_id');
    }

    
}
