<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Base;
use App\Models\Product;
use App\Models\User;


class Order extends Model
{
    use Base;

    protected $primaryKey = 'id';
    protected $table = 'orders';

    protected $fillable = [
        'status',
        'required_date',
        'shipped_date',
        'customer_id',
        'order_number',
        'comments'
    ];


    protected $validationRules = [
        'status' => 'required|string',
        'required_date' => 'nullable|date',
        'shipped_date' => 'nullable|date',
        'customer_id' => 'required|integer',
    ];


    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_products')->withPivot('quantity_ordered', 'amount');;
    }
    
    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'order_id');
    }


    public function toArray($data = array()) 
    {
        $data = parent::toArray();

        $oCustomer = User::where('id', $this->customer_id)->first();
        
        $sTotalOrderAmount = \DB::table('orders')->select('order_products.amount')
        ->join('order_products', 'order_products.order_id', '=', 'orders.id')
        ->where('orders.id','=',$this->id)
        ->sum('order_products.amount');
        
        $data['total_amount'] = $sTotalOrderAmount;
        $data['customer_name'] = $oCustomer->fullName();
    
        return $data;
    }
}
