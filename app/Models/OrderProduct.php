<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Base;


class OrderProduct extends Model
{
    use Base;
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'order_products';

    protected $fillable = [
        'order_id',
        'product_id',
        'quantity_ordered',
        'amount',
    ];

    protected $validationRules = [
        'order_id' => 'required|integer',
        'product_id' => 'required|integer',
        'quantity_ordered' => 'required|integer',
        'amount' => 'required|numeric',
    ];

    
}
