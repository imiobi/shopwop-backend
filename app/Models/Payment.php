<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Base;
use App\Models\PaymentMethod;
use App\Models\Order;


class Payment extends Model
{
    use Base;
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'payments';

    protected $fillable = [
        'order_id',
        'payment_method_id',
        'total_amount',
        'payment_date',
        'is_paid'
    ];

    protected $validationRules = [
        'order_id' => 'required|integer',
        'payment_method_id' => 'required|integer',
        'total_amount' => 'required|numeric',
        'payment_date' => 'nullable|date',
        'is_paid' => 'required|boolean',
    ];


    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method_id', 'id');
    }

    public function order()
    {
        $this->belongsTo(Order::class, 'order_id');
    }

    public function toArray($data = array())
    {
        $data = parent::toArray();
        $data['payment_method_name'] = $this->paymentMethod()->select('payment_method_name')->pluck('payment_method_name')->first();
        $data['is_paid'] = ($this->is_paid ===  1) ? 'Paid' : 'Not Paid';

        return $data;
    }
    
}
