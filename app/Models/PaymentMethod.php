<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Base;
use App\Models\Payment;



class PaymentMethod extends Model
{
    use Base;
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'payment_methods';

    protected $fillable = [
        'first_name',
        'last_name',
        'age'
    ];

    protected $validationRules = [
        'payment_method_id' => 'required|integer',
        'charges' => 'required|numeric',
        'is_active' => 'required|boolean'
    ];

    public function payments()
    {
        $this->hasMany(Payment::class, 'payment_method_id', 'id');
    }


    
}
