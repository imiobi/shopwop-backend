<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use App\Models\Base;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Cart;
use App\Models\Order;
use App\Models\WishList;

class Product extends Model
{
    use Base;
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'products';

    protected $fillable = [
        'product_title',
        'product_description',
        'short_description',
        'category_id',
        'stock_quantity',
        'brand_id',
        'price',
        'variants',
        'is_active',
        'product_code',
        'specifications'
    ];

    protected $validationRules = [
        'product_title' => 'required',
        'product_description' => 'required',
        'short_description' => 'required',
        'category_id' => 'required|integer',
        'stock_quantity' => 'required|integer|min:0',
        'brand_id' => 'required|integer',
        'price' => 'required|numeric|min:0',
        'variants' => 'nullable|string',
        'is_active' => 'required|boolean',
        'specifications' => 'required'
    ];

    public function attachments()
    {
        return $this->hasMany(Attachment::class, 'product_id', 'id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'product_tags');
    }
    
    public function carts()
    {
        return $this->hasMany(Cart::class, 'product_id');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_products');
    }

    public function wishListUsers()
    {
        return $this->belongsToMany(User::class, 'wish_lists', 'product_id', 'customer_id');
    }

    public function toArray($data = array())
    {
        $data = parent::toArray();
        $data['category_name'] = Category::where('id', $this->category_id)->pluck('category_name')->first();
        $data['brand_name'] = Brand::where('id', $this->brand_id)->pluck('brand_name')->first();
        $data['image'] = $this->attachments()->get();

        return $data;
    }


}
