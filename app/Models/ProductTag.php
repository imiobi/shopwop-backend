<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductTag extends Model
{
    use Base;
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'product_tags';

    protected $fillable = [
        'product_id',
        'tag_id'
    ];

    
    protected $validationRules = [
        'product_id' => 'required|numeric',
        'tag_id' => 'required|numeric'
    ];
    
    
}
