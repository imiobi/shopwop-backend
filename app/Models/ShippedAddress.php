<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Base;
use App\Models\User;


class ShippedAddress extends Model
{

    use Base;
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'shipped_address';

    protected $fillable = [
        'customer_id',
        'first_name',
        'last_name',
        'company',
        'street_address',
        'city',
        'province',
        'country',
        'contact_no',
        'postal_code',
    ];

    protected $validationRules = [
        'customer_id' => 'required|integer',
        'first_name' => 'required',
        'last_name' => 'required',
        'street_address' => 'required',
        'city' => 'required',
        'province' => 'required',
        'country' => 'required',
        'contact_no' => 'required|integer',
        'postal_code' => 'required|integer|min:3|max:10',
    ];
    

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }
    
}
