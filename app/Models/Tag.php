<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Base;
use App\Models\Product;

class Tag extends Model
{
    use Base;
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $table = 'tags';

    protected $fillable = [
        'tag_name'
    ];

    protected $validationRules = [
        'customer_id' => 'required|integer'
    ];
    

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_tags');
    }

    
    
}
