<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use App\Models\ShippedAddress;
use App\Models\Cart;
use App\Models\WishList;
use App\Models\Order;
use App\Models\Base;
use Illuminate\Validation\Rule; 

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
    use Base;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'date_of_birth',
        'company',
        'email',
        'contact_no',
        'state',
        'city',
        'postal_code',
        'address',
        'shipped_address',
    ];


    protected $validationRules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|email|unique:users,email',
        'contact_no' => 'required',
        'state' => 'required',
        'city' => 'required',
        'postal_code' => 'required|numeric'
    ];
    

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', '_method'
    ];

    public function getRulesForUpdate()
    {
        $this->validationRules['email'] =  [
                                                'required',
                                                Rule::unique('users', 'email')->ignore($this->id),
                                            ];

        return $this->validationRules;
    }

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function shippedAddresses()
    {
        return $this->hasMany(ShippedAddress::class, 'customer_id');
    }

    public function carts()
    {
        return $this->hasMany(Cart::class, 'customer_id');
    }

    public function wishListProducts()
    {
        return $this->belongsToMany(Product::class, 'wish_lists', 'customer_id', 'product_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'customer_id');
    }



}
