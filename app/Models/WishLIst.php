<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Base;
use App\Models\User;
use App\Models\Product;


class WishList extends Model
{
    use Base;   


    protected $primaryKey = 'id';
    protected $table = 'wish_lists';

    protected $fillable = [
        'product_id',
        'customer_id',
    ];

    protected $validationRules = [
        'product_id' => 'required|integer',
        'customer_id' => 'required|integer'
    ];
    

  
}
