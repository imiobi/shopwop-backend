<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 30);
            $table->string('last_name', 30);
            $table->date('date_of_birth')->nullable();
            $table->string('company', 30)->nullable();
            $table->string('email', 40);
            $table->text('password');
            $table->boolean('is_admin')->default(0);
            $table->string('contact_no');
            $table->string('state');
            $table->string('city');
            $table->unsignedInteger('postal_code');
            $table->string('shipped_address');
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
