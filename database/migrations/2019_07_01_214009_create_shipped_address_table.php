<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippedAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipped_address', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned()->default(null);
            $table->string('first_name', 30);
            $table->string('last_name', 30);
            $table->string('company', 40)->nullable();
            $table->text('street_address');
            $table->string('city');
            $table->string('province');
            $table->string('country');
            $table->string('contact_no');
            $table->unsignedInteger('postal_code');
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipped_address');
    }
}
