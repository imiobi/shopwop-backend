<?php

use Illuminate\Database\Seeder;
use App\Models\Brand;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i<=10; $i++) {
            Brand::insert([
                'brand_name' => "brand $i",
                'contact_no' => '03211234567',
                'email' => "brand$i@brand.com",
                'country' => 'Pakistan',
            ]);
        }
    }
}
