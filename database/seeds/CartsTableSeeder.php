<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Product;
use App\Models\Cart;
use App\Models\User;

class CartsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aCustomer = User::pluck('id')->where('is_admin', 0)->toArray();
        $aProducts = Product::select('id')->where('is_active', 1)->pluck('id')->toArray();


        for($i=0; $i<=30; $i++) {
            Cart::insert([
                'customer_id' => array_random($aCustomer),
                'product_id' => array_random($aProducts),
                'created_at' => Carbon::now(),
            ]);
        }
    }
}
