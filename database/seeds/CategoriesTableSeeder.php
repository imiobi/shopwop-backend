<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 4; $i++) {
            Category::insert([
                'category_name'=> "category $i",
                'parent_category_id'=> null,
                'description'=> "Lorem Ipsum dollar sit amet",
                'is_active'=> random_int(0,1),
                'created_at'=> Carbon::now(),
            ]);
        }
    }
}
