<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderProduct;
use Carbon\Carbon;


class OrderProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aProducts = Product::pluck('price','id')->toArray();
        $aOrders = Order::pluck('id')->toArray();

        for($i = 0; $i<40; $i++) {
            $iProductID = array_rand($aProducts);
            $iQuantity = random_int(1,10);

            OrderProduct::insert([
                'order_id' => array_random($aOrders),
                'product_id' => $iProductID,
                'quantity_ordered' => $iQuantity,
                'amount' => $aProducts[$iProductID] * $iQuantity,
                'created_at' => Carbon::now(),
            ]);
        }
    }
}
