<?php

use Illuminate\Database\Seeder;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aCustomerId = App\Models\User::pluck('id')->where('is_admin', 0)->toArray();

        $aStatues = ['Completed', 'Pending', 'Cancelled', 'Dispatch', 'On Hold'];

        for($i=0; $i<40; $i++) {
            $status = array_random($aStatues);
            Order::insert([
                'order_number' => "#ORD".date('mdy')."-$i",
                'status' => $status,
                'customer_id' => array_random($aCustomerId),
                'required_date' => date('y:m:d', strtotime("+2 days")),
                'shipped_date' => ($status == 'Completed') ? date('y:m:d', strtotime("+1 days")) :  null ,
                'comments' => "Lorem Ipsum dollar sit amet",
                'created_at' => Carbon::now()
            ]);
        }
    }
}
