<?php

use Illuminate\Database\Seeder;
use App\Models\PaymentMethod;
use Illuminate\Support\Carbon;

class PaymentMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentMethod::insert([
            'payment_method_name'=> "Cash On Delivery",
            'charges'=> 100,
            'is_active'=> 1,
            'created_at'=> Carbon::now(),
        ]);
    }
}
