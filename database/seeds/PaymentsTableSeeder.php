<?php

use Illuminate\Database\Seeder;
use Doctrine\DBAL\Schema\Schema;
use App\Models\Payment;
use App\Models\Order;
use Illuminate\Support\Carbon;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $aOrders =  Order::pluck('id')->toArray();

        for($i= 0; $i < 50; $i++) {
            Payment::insert([
                'order_id'=> array_random($aOrders),
                'payment_method_id'=> 1,
                'total_amount'=> rand(1000, 30000),
                'payment_date'=> null,
                'is_paid'=> 0,
                'created_at'=> Carbon::now(),
            ]);
        }
        
    }
}
