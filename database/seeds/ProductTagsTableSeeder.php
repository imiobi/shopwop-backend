<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Tag;
use Illuminate\Support\Carbon;
use App\Models\ProductTag;

class ProductTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aProducts = Product::pluck('id')->toArray();
        $aTags = Tag::pluck('id')->toArray();
        
        for($i = 0; $i < 40; $i++) {

            ProductTag::insert([
                'product_id'=> array_random($aProducts),
                'tag_id'=> array_random($aTags),
                'created_at'=> Carbon::now(),
            ]);
        }
    }
}
