<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Category;
use Carbon\Carbon;


class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aCategories = App\Models\Category::pluck('id')->toArray();
        $aBrands = App\Models\Brand::pluck('id')->toArray();

        for($i=0; $i<30; $i++) {
            Product::insert([
                'product_code' => "#PRD-$i",
                'product_title' => "product $i",
                'product_description' => 'Lorem Ipsum dollar sit amet',
                'short_description' => 'Lorem Ipsum',
                'category_id' => array_random($aCategories),
                'stock_quantity' => random_int(0,100),
                'brand_id' => array_random($aBrands),
                'price' => random_int(99,9999),
                'variants' => 'L,M,XL,XXL',
                'view_count' => random_int(20,999),
                'is_active' => random_int(0, 1),
                'created_at' => Carbon::now()
            ]);
        }
    }
}
