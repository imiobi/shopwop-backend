<?php

use Illuminate\Database\Seeder;
use App\Models\Tag;
use Carbon\Carbon;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aTags = ['jeans', 'shirts', 'sports', 'phones', 'samsung','iphone', 't-shirts', 't-shirt'];

        foreach($aTags as $sTag) {
            Tag::insert([
                'tag_name'=> $sTag,
                'created_at'=> Carbon::now(),
            ]); 
        }
    }
}
