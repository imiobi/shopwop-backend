<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aCities = ['karachi', 'hyderabad', 'larkana', 'mirpur'];

        for($i = 0; $i <= 20; $i++) {
            User::insert([
                'first_name' => 'user',
                'last_name' => $i,
                'date_of_birth' => '1994-09-11',
                'company' => "company $i",
                'email' => "admin$i@example.com",
                'password' => 'admin123',
                'is_admin' => random_int(0,1),
                'contact_no' => '03001234567',
                'state' => 'Sindh',
                'city' =>  array_random($aCities),
                'postal_code' => '12345',
                'shipped_address' => "Po Box 336, Grundy Center, IA",
                'created_at' => Carbon::now()
            ]);
        }
    }
}
