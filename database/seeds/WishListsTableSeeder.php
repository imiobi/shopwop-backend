<?php

use Illuminate\Database\Seeder;
use App\Models\WishList;
use App\Models\User;
use App\Models\Product;
use Carbon\Carbon;

class WishListsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aCustomers = User::pluck('id')->where('is_admin', '=' ,0)->toArray();
        $aProducts = Product::select('id')->where('is_active', 1)->pluck('id')->toArray();


        for($i = 0; $i < 40; $i++) {
            WishList::insert([
                'product_id'=> array_random($aProducts),
                'customer_id'=> array_random($aCustomers),
                'created_at'=> Carbon::now(), 
            ]);
        }
    }
}
