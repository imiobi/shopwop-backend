<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});




$router->group(['prefix' => 'api/v1'], function() use ($router) {

    $router->group(['prefix' => 'products'], function() use ($router) {

        $router->get('/', 'ProductController@index');
        $router->get('index', 'ProductController@index');
        $router->get('show/{id}', 'ProductController@show');
        $router->delete('delete/{id}', 'ProductController@delete');
        $router->post('create', 'ProductController@create');
        $router->put('update/{id}', 'ProductController@update');
        $router->get('/brand-with-category', 'ProductController@getBrandAndCategory');
        $router->get('/wish-lists/{userId}', 'ProductController@wishLists');
        $router->get('top-4-recent-products', 'ProductController@top4RecentProducts');
        $router->get('top-12-most-viewed-products', 'ProductController@top12ViewedProducts');
        $router->get('top-4-sold-products', 'ProductController@top4SoldProducts');
        $router->get('/search', 'ProductController@searchProducts');
        $router->get('/stats', 'ProductController@productsStats');

    });


    $router->group(['prefix' => 'brands'], function() use ($router) {
        $router->get('/', 'BrandController@index');
        $router->get('index', 'BrandController@index');
        $router->get('show/{id}', 'BrandController@show');
        $router->delete('delete/{id}', 'BrandController@delete');
        $router->post('/create', 'BrandController@create');
        $router->put('update/{id}', 'BrandController@update');
        $router->get('count-products-by-brand', 'BrandController@countProductsByBrand');
        $router->get('stats', 'BrandController@getBrandsStats');


    });

    $router->group(['prefix' => 'categories'], function() use ($router) {
        $router->get('/', 'CategoryController@index');
        $router->get('index', 'CategoryController@index');
        $router->get('show/{id}', 'CategoryController@show');
        $router->delete('delete/{id}', 'CategoryController@delete');
        $router->post('create', 'CategoryController@create');
        $router->put('update/{id}', 'CategoryController@update');
        $router->get('count-products-by-category', 'CategoryController@countProductsByCategory');
        $router->get('top-5-categories', 'CategoryController@top5Categories');
        $router->get('stats', 'CategoryController@getCategoriesStats');
        $router->get('{categoryId}/products', 'CategoryController@productsByCategory');
    });



    $router->group(['prefix' => 'orders'], function() use ($router) {
        $router->get('/', 'OrderController@index');
        $router->get('index', 'OrderController@index');
        $router->get('show/{id}', 'OrderController@show');
        $router->post('create', 'OrderController@create');
        $router->put('update/{id}', 'OrderController@update');
        $router->get('statuses', 'OrderController@getOrderStatuses');
        $router->get('order-status-count', 'OrderController@getOrderStatusCount');
    });


    $router->group(['prefix' => 'carts'], function() use ($router) {
        $router->delete('delete/{cartId}', 'CartController@delete');
        $router->post('add-to-cart', 'CartController@addCart');
        $router->get('carts/{customerId}', 'CartController@cartListing');
        $router->put('update-cart', 'CartController@updateCart');
    });

    $router->group(['prefix' => 'payments'], function() use ($router){
        $router->get('/', 'PaymentController@index');
        $router->get('index', 'PaymentController@index');
    });

    $router->group(['prefix' => 'users'], function() use ($router){
        $router->get('{type}', 'UserController@listUsers');

        $router->get('show/{id}', 'UserController@show');
        $router->put('update/{id}', 'UserController@update');
        $router->delete('delete/{id}', 'UserController@delete');
        $router->get('order-history/{customerId}', 'UserController@orderHistory');
    });

    $router->group(['prefix'=> 'wishlist'], function() use ($router) {

        $router->post('create', 'WishListController@storeWishList');
        $router->get('/{id}', 'WishListController@index');
        $router->delete('delete', 'WishListController@delete');
    });


    
});